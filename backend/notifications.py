# -*- coding: utf-8 -*-
from backend.settings import FCM
from pyfcm import FCMNotification


def send(devices, message, action=None, title=None):
    sender = FCMNotification(api_key=FCM['apiKey'])
    response = False
    data = {
        "message": message
    }

    try:
        if title:
            data['title'] = title

        if action:
            data['type'] = action
        else:
            data['type'] = 'notification'

        print data
        print devices
        print len(devices)

        if not isinstance(devices, list):
            num = 1
            result = sender.notify_single_device(registration_id=devices, data_message=data)
        elif len(devices) == 1:
            num = 1
            result = sender.notify_single_device(registration_id=devices[0], data_message=data)
        else:
            num = len(devices)
            result = sender.notify_multiple_devices(registration_ids=devices, data_message=data)

        print result
        if result['success'] != 0:
            response = {
                'total': num,
                'success': result['success'],
                'failure': result['failure']
            }

    except Exception as e:
        print e.message

    return response
