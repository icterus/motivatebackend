from django.conf.urls import include, url
from django.contrib import admin
from motivate.views import error404

from django.conf import settings as config
from django.conf.urls.static import static


urlpatterns = [
    ### API ###
    url(r'^api/', include('api.urls')),
    url(r'^admin/', include(admin.site.urls)),
] + static(config.STATIC_URL, document_root=config.STATIC_ROOT)

handler404 = error404
handler500 = error404
