from django.contrib import admin
from models import *

admin.site.register(Users)
admin.site.register(Teams)
admin.site.register(Rewards)
admin.site.register(Cycles)
admin.site.register(Prize)
admin.site.register(Config)