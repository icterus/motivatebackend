# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motivate', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='prize',
            name='image',
            field=models.CharField(max_length=150, blank=True),
        ),
        migrations.AlterField(
            model_name='images',
            name='image',
            field=models.ImageField(upload_to=b'static/uploads'),
        ),
        migrations.AlterField(
            model_name='users',
            name='avatar',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
