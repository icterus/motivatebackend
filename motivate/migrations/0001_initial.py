# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Answers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('answer', models.CharField(max_length=150)),
                ('created_at', models.DateField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Config',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('company_identity', models.TextField(verbose_name=b'Identidad Corporativa')),
                ('company_identity_other', models.TextField(verbose_name=b'Identidad Corporativa')),
            ],
            options={
                'verbose_name': 'Configuraci\xf3n de la Aplicaci\xf3n',
                'verbose_name_plural': 'Configuraci\xf3n de la Aplicaci\xf3n',
            },
        ),
        migrations.CreateModel(
            name='Cycles',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('expire', models.DateTimeField()),
                ('updated_in', models.DateTimeField(auto_now_add=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('status', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Devices',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('token', models.TextField()),
                ('created_at', models.DateField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Goals',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('type', models.SmallIntegerField(default=1, choices=[(1, 'Selecci\xf3n Simple'), (2, b'Textual')])),
                ('score', models.SmallIntegerField(default=1)),
                ('cycles', models.ForeignKey(to='motivate.Cycles')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Images',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=b'uploads')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Prize',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=50)),
                ('created_at', models.DateField(auto_now_add=True)),
                ('img', models.URLField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Rewards',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.CharField(max_length=250)),
                ('created_at', models.DateField(auto_now_add=True)),
                ('creator', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('cycle', models.ForeignKey(to='motivate.Cycles')),
                ('prize', models.ForeignKey(to='motivate.Prize')),
            ],
        ),
        migrations.CreateModel(
            name='Teams',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('company', models.SmallIntegerField(default=1, choices=[(1, b'Solinca'), (2, b'Proyectos y Desarrollo 636')])),
                ('avatar', models.ImageField(default=b'', upload_to=b'avatars/')),
                ('position', models.SmallIntegerField(default=1, choices=[(1, b'Empleado'), (2, b'Supervisor'), (3, b'Staff')])),
                ('team', models.ForeignKey(to='motivate.Teams')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='rewards',
            name='user',
            field=models.ForeignKey(to='motivate.Users'),
        ),
        migrations.AddField(
            model_name='devices',
            name='user',
            field=models.ForeignKey(to='motivate.Users'),
        ),
        migrations.AddField(
            model_name='answers',
            name='goal',
            field=models.ForeignKey(to='motivate.Goals'),
        ),
        migrations.AddField(
            model_name='answers',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
