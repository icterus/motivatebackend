# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motivate', '0002_auto_20170422_0001'),
    ]

    operations = [
        migrations.CreateModel(
            name='GroupAnswers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('answer', models.CharField(max_length=150)),
                ('created_at', models.DateField(auto_now_add=True)),
                ('goal', models.ForeignKey(to='motivate.Goals')),
                ('team', models.ForeignKey(to='motivate.Teams')),
            ],
        ),
        migrations.AlterField(
            model_name='users',
            name='avatar',
            field=models.CharField(max_length=100, blank=True),
        ),
    ]
