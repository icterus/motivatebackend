# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motivate', '0003_auto_20170424_0329'),
    ]

    operations = [
        migrations.AddField(
            model_name='answers',
            name='indicted',
            field=models.BooleanField(default=False),
        ),
    ]
