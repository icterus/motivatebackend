# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motivate', '0004_answers_indicted'),
    ]

    operations = [
        migrations.CreateModel(
            name='Improvements',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('job', models.TextField(max_length=100)),
                ('justify', models.TextField(max_length=250)),
                ('courses', models.TextField(max_length=250)),
                ('created_at', models.DateField(auto_now_add=True)),
                ('cycle', models.ForeignKey(to='motivate.Cycles')),
                ('user', models.ForeignKey(to='motivate.Users')),
            ],
        ),
        migrations.CreateModel(
            name='Innovation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('process', models.TextField()),
                ('supervisor', models.TextField()),
                ('company', models.TextField()),
                ('created_at', models.DateField(auto_now_add=True)),
                ('cycle', models.ForeignKey(to='motivate.Cycles')),
                ('user', models.ForeignKey(to='motivate.Users')),
            ],
        ),
    ]
