# -*- coding: utf-8 -*-

from django.contrib.auth.models import User, Group
from django.db import models
from model_utils import Choices
from backend.settings import UPLOAD_ROOT

COMPANY = Choices((1, 'Solinca', ('Solinca')), (2, 'Proyectos y Desarrollo 636', ('Proyectos y Desarrollo 636')))
ROLS = Choices((1, 'admin', ('Administrador')), (2, 'supervisor', ('Supervisor')), (3, 'user', ('Usuario')))
POSITION = Choices((1, 'employer', ('Empleado')), (2, 'supervisor', ('Supervisor')), (3, 'staff', ('Staff')))
TYPE = Choices((1, 'simple', (u'Selección Simple')), (2, 'text', ('Textual')))


class BaseModel(models.Model):
    class Meta:
        abstract = True

    def __unicode__(self):
        return self.name


class Config(models.Model):
    company_identity = models.TextField(verbose_name="Identidad Corporativa")
    company_identity_other = models.TextField(verbose_name="Identidad Corporativa")

    def __unicode__(self):
        return u'Configuración Única'

    class Meta:
        verbose_name = u"Configuración de la Aplicación"
        verbose_name_plural = u"Configuración de la Aplicación"


class Cycles(BaseModel):
    name = models.CharField(max_length=50)
    expire = models.DateTimeField()
    updated_in = models.DateTimeField(auto_now_add=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    status = models.BooleanField(default=True, blank=True)


class Goals(BaseModel):
    name = models.CharField(max_length=50)
    type = models.SmallIntegerField(default=1, choices=TYPE)
    score = models.SmallIntegerField(default=1)
    cycles = models.ForeignKey(Cycles)


class Answers(models.Model):
    answer = models.CharField(max_length=150)
    goal = models.ForeignKey(Goals)
    user = models.ForeignKey(User)
    indicted = models.BooleanField(default=False, blank=True)
    created_at = models.DateField(auto_now_add=True)

class Teams(BaseModel):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name

class GroupAnswers(models.Model):
    answer = models.CharField(max_length=150)
    goal = models.ForeignKey(Goals)
    team = models.ForeignKey(Teams)
    created_at = models.DateField(auto_now_add=True)

class Users(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    company = models.SmallIntegerField(choices=COMPANY, default=COMPANY.Solinca)
    avatar = models.CharField(max_length=100, blank=True)
    team = models.ForeignKey(Teams)
    position = models.SmallIntegerField(choices=POSITION, default=POSITION.employer)

    @property
    def full_name(self):
        return self.user.first_name + ' ' + self.user.last_name

    def __unicode__(self):
        return self.full_name


class Prize(BaseModel):
    image = models.CharField(max_length=150, blank=True)
    name = models.CharField(max_length=50, unique=True)
    created_at = models.DateField(auto_now_add=True)
    img = models.URLField()


class Rewards(models.Model):
    user = models.ForeignKey(Users)
    cycle = models.ForeignKey(Cycles)
    prize = models.ForeignKey(Prize)
    text = models.CharField(max_length=250)
    creator = models.ForeignKey(User, null=True, blank=True)
    created_at = models.DateField(auto_now_add=True)


class Images(models.Model):
    image = models.ImageField(upload_to=UPLOAD_ROOT)
    created_at = models.DateTimeField(blank=True, null=True, auto_now_add=True)


class Devices(models.Model):
    token = models.TextField()
    user = models.ForeignKey(Users)
    created_at = models.DateField(auto_now_add=True)


class Innovation(models.Model):
    user = models.ForeignKey(Users)
    process = models.TextField()
    supervisor = models.TextField()
    company = models.TextField()
    cycle = models.ForeignKey(Cycles)
    created_at = models.DateField(auto_now_add=True)

class Improvements(models.Model):
    user = models.ForeignKey(Users)
    job = models.TextField(max_length=50)
    justify = models.TextField(max_length=250)
    courses = models.TextField(max_length=250)
    cycle = models.ForeignKey(Cycles)
    created_at = models.DateField(auto_now_add=True)
