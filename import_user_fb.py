# -*- coding: utf-8 -*-

from motivate.models import Users
from api.helper import firebase

users = Users.objects.all()
total = len(users)
imported = 0

fb = firebase()

for user in users:
    fb.create_user(user)
    imported += 1
    print('%s (%s/%s)' % (user.user.get_full_name(), imported, total))
