# -*- coding: utf-8 -*-

from django.contrib.auth import authenticate
from django.http import JsonResponse

from motivate.models import *
from backend.notifications import send
from django.views.decorators.csrf import csrf_exempt
from django.db import transaction
from django.db.models import Count
from django.db.utils import *
from django.core.mail import send_mail
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings

from querystring_parser import parser

from backend.utils import randomword

from jose import jwt
from backend.settings import SECRET_KEY
from api.decorators import jwt_check
from api.helper import firebase


def treatment_user(person):
    try:
        avatar_url = ''
        if person.avatar:
            avatar_url = person.avatar

        data = {
            'id': int(person.user.id),
            'username': person.user.username,
            'name': person.user.first_name + ' ' + person.user.last_name,
            'first_name': person.user.first_name,
            'last_name': person.user.last_name,
            'email': person.user.email,
            'company': person.company,
            'avatar': avatar_url,
            'position': person.position,
            'position_name': person.get_position_display(),
            'password': '',
            'repassword': '',
            'team': {
                'id': person.team.id,
                'name': person.team.name
            }
        }
    except Exception as e:
        print(e.message)

    return data


def treatment_cycle(cycle):
    try:
        objects = cycle.goals_set.all()

        score_total = 0
        goals = []
        for i in objects:
            score_total += i.score
            body = {
                'name': i.name,
                'type': i.type,
                'score': i.score
            }
            goals.append(body)

        data = {
            'id': cycle.id,
            'name': cycle.name,
            'expire': cycle.expire,
            'goals': goals,
            'score': score_total,
            'created_at': cycle.created_at,
            'updated_in': cycle.updated_in,
            'status': cycle.status
        }

    except Exception as e:
        print(e)
        return None

    return data


def treatment_points(cycles_id):
    cycles_query = Cycles.objects.all()
    cycles = []
    for item in cycles_query:
        status = 'Inactivo'
        if item.status:
            status = 'Activo'

        selected = 0
        if int(item.id) == int(cycles_id):
            selected = 1

        c = {
            'id': item.id,
            'name': item.name,
            'status': status,
            'selected': selected
        }
        cycles.append(c)

    goals_query = Goals.objects.filter(cycles_id=cycles_id)
    goals = []
    for item in goals_query:
        goal = {
            'id': item.id,
            'name': item.name,
            'score': item.score,
            'type': item.type
        }
        goals.append(goal)

    users_query = Users.objects.filter(user__is_active=True).exclude(position=3)
    users = []
    for item in users_query:
        user = {
            'id': item.user.id,
            'name': item.user.get_full_name(),
            'team': item.team.name,
            'team_id': item.team_id,
            'position': item.position,
            'image': item.avatar,
            'email': item.user.email
        }
        users.append(user)

    teams_query = Teams.objects.all()
    teams = []
    for item in teams_query:
        team = {
            'id': item.id,
            'name': item.name
        }
        teams.append(team)

    teams_answer = GroupAnswers.objects.filter(goal__cycles_id=cycles_id)
    groups = []
    for item in teams_answer:
        answer = {
            'id': item.id,
            'value': item.goal.score,
            'goal_id': item.goal.id,
            'answer': item.answer,
            'team_id': item.team.id,
        }
        groups.append(answer)

    person_answer = Answers.objects.filter(goal__cycles_id=cycles_id)
    persons = []
    for item in person_answer:
        answer = {
            'id': item.id,
            'value': item.goal.score,
            'goal_id': item.goal.id,
            'answer': item.answer,
            'user_id': item.user_id,
            'state': item.indicted
        }
        persons.append(answer)

    data = {
        'cycles': cycles,
        'users': users,
        'goals': goals,
        'teams': teams,
        'unit_answer': persons,
        'team_answer': groups
    }

    return data


def calc_point_cycle(cycle_id, user):
    goals = Goals.objects.filter(cycles_id=cycle_id)
    answers = Answers.objects.filter(goal__cycles_id=cycle_id, user__id=user.id).exclude(indicted=True)
    team_answers = GroupAnswers.objects.filter(goal__cycles_id=cycle_id, team_id=user.users.team.id)

    final_score = 0
    for goal in goals:
        goal_point = 0
        goal_point_team = 0

        for answer in answers:
            if answer.goal_id == goal.id:
                if answer.answer != 0 and answer.answer != '':
                    goal_point = goal.score
                break

        for answer in team_answers:
            if answer.goal_id == goal.id:
                if answer.answer != 0 and answer.answer != '':
                    goal_point_team = goal.score
                break

        final_score += (goal_point + goal_point_team) / 2

    return float('%.2f' % final_score)


def calc_point_goal(goal, user):
    answer = Answers.objects.filter(goal__cycles_id=goal.cycles_id, goal_id=goal.id, user_id=user.user_id)\
                .exclude(indicted=True)
    team_answer = GroupAnswers.objects.filter(goal__cycles_id=goal.cycles_id, goal_id=goal.id, team_id=user.team.id)

    points = 0
    team_points = 0

    if len(answer):
        if answer[0].answer != 0 and answer[0].answer != '':
            points = goal.score

    if len(team_answer):
        if team_answer[0].answer != 0 and team_answer[0].answer != '':
            team_points = goal.score

    final = (points + team_points) / 2
    return float('%.2f' % final)


@csrf_exempt
def account(request, uid):
    response = {
        'status': 'error',
        'message': 'Usuario no existe'
    }

    try:
        person = Users.objects.get(user__username=uid)
        avatar_url = ''
        if person is not None:
            if person.avatar:
                avatar_url = person.avatar

            response = {
                'status': 'ok',
                'account': {
                    'uid': uid,
                    'name': person.user.first_name + ' ' + person.user.last_name,
                    'company': person.company,
                    'avatar': avatar_url
                }
            }
    except Exception as e:
        print e.message

    return JsonResponse(response)


@csrf_exempt
def auth(request):
    response = {
        'status': 'error',
        'message': 'Usuario o Contraseña inválido'
    }

    try:
        uid = request.POST['uid']
        password = request.POST['password']
        user = authenticate(username=uid, password=password)
        if user:
            users = Users.objects.get(user__username=uid)
            data = treatment_user(users)

            response['person'] = data
            response['status'] = 'ok'
            del response['message']

            token = {
                'id': users.user.id,
                'email': users.user.email,
                'name': users.user.get_full_name(),
                'company': users.company,
                'position': users.position,
                'team': users.team.id,
                'code': randomword(10)
            }
            response['token'] = jwt.encode(token, SECRET_KEY, algorithm='HS512')

    except Exception as e:
        print e.message

    return JsonResponse(response)


@jwt_check
def legal(request):
    response = {
        'status': 'error',
        'message': 'Error obteniendo datos'
    }

    try:
        text = Config.objects.get(id=1)
        response = {
            'status': 'ok',
            'legal_text': text.company_identity,
            'legal_text_other': text.company_identity_other
        }
    except Exception as e:
        print e.message

    return JsonResponse(response)


@csrf_exempt
@jwt_check
def edit_legal(request):
    response = {
        'status': 'error',
        'message': 'Error obteniendo datos'
    }

    try:
        config = Config.objects.get(id=1)
        config.company_identity = request.POST['text']
        config.company_identity_other = request.POST['text_other']
        config.save()

        response['status'] = 'ok'
        response['message'] = 'Datos actualizados correctamente'
    except Exception as e:
        print e.message

    return JsonResponse(response)


@jwt_check
def register_devices(request, token):
    response = {
        'status': 'error',
        'message': 'Error registrando dispositivos'
    }
    try:
        user_id = request.META['JWT_AUTH']['id']
        devices = Devices.objects.filter(token=token)
        if not devices:
            devices = Devices(token=token)
            devices.user_id = user_id
            devices.save()

        response['status'] = 'ok'
        response['message'] = 'Dispositivo registrado exitosamente'
    except Exception as e:
        print e.message

    return JsonResponse(response)


@jwt_check
def update_data(request, uid):
    response = {
        'status': 'error',
        'message': 'Usuario no inválido'
    }
    try:
        data = treatment_user(Users.objects.get(pk=uid))

        response['person'] = data
        response['status'] = 'ok'
        del response['message']

    except Exception as e:
        print e.message

    return JsonResponse(response)


#@jwt_check
def forget(request, uid):
    response = {
        'status': 'error',
        'message': u'Este identificador de usuario no coincide con ningún usuario registrado'
    }
    try:
        user = User.objects.get(username=uid)
        # user.users.team.departments
        if user is not None:
            username, company = user.email.split('@')
            pre = []
            for letter in username:
                if len(pre) < 4:
                    pre.append(letter)
                else:
                    pre.append('*')

            hidden = ''.join(pre)
            new_key = randomword(6)
            user.set_password(new_key)
            user.save()

            plain_message = u"""
                Señor(a) %s %s

                Su solicitud de reinicio de contraseña ha sido éxitosa,
                por lo que su nueva contraseña dentro de la aplicación es: %s
            """ % (user.first_name, user.last_name, new_key)
            html_message = u"""
                <p>
                  Señor(a) %s %s
                </p>

                <p>
                  Su solicitud de reinicio de contraseña ha sido éxitosa,
                  por lo que su nueva contraseña dentro de la aplicación es <b>%s</b>
                </p>
            """ % (user.first_name, user.last_name, new_key)

            send_mail('Correo de reinicio de clave de Motivate',
                      plain_message,
                      'no responder <%s>' % getattr(settings, 'EMAIL_HOST_USER'),
                      [user.email],
                      html_message=html_message,
                      fail_silently=False)

            response['status'] = 'ok'
            response['message'] = u'Ha sido enviado un correo para recuperación de contraseña a %s@%s' % (hidden, company)

    except Exception as e:
        print e

    return JsonResponse(response)


@jwt_check
def get_goals(request):
    response = {
        'status': 'error',
        'message': u'Error al obtener las metas del usuario'
    }

    try:
        user_id = request.META['JWT_AUTH']['id']
        cycle = Cycles.objects.filter(status=True).first()

        if not cycle:
            response['message'] = 'No hay ningún ciclo de evaluación activo'
            raise Exception(response['message'])

        goals = Goals.objects.filter(cycles_id=cycle.id)

        del response['message']
        response['status'] = 'ok'
        response['cycle'] = cycle.name
        response['expire'] = cycle.expire
        response['goals'] = []

        for goal in goals:
            answers = Answers.objects.filter(user_id=user_id, goal_id=goal.id).first()
            if answers:
                answer = True
            else:
                answer = None

            item = {
                'id': goal.id,
                'type': goal.type,
                'question': goal.name,
                'answer': answer
            }

            response['goals'].append(item)
    except Exception as e:
        print(e)

    return JsonResponse(response)


@jwt_check
def prize(request):
    response = {
        'status': 'error',
        'message': u'Error no se puede recuperar'
    }

    try:
        uid = request.META['JWT_AUTH']['id']

        rewards = Rewards.objects.filter(user_id=uid)

        del response['message']
        response['status'] = 'ok'
        response['items'] = []

        if len(rewards):
            for reward in rewards:
                item = {
                    'id': reward.prize.id,
                    'name': reward.prize.name,
                    'date': reward.created_at,
                    'cycle': reward.cycle.name
                }
                response['items'].append(item)

    except Exception as e:
        print(e)

    return JsonResponse(response)


# Nuevo API
@jwt_check
def all_prizes(request):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }
    try:
        items = Prize.objects.all()
        response['status'] = 'ok'
        response['items'] = []
        del response['message']
        if items:
            for item in items:
                data = {
                    'id': item.id,
                    'name': item.name,
                    'image': item.image,
                    'created_at': item.created_at
                }
                response['items'].append(data)

    except Exception as e:
        print e.message

    return JsonResponse(response)


@jwt_check
@csrf_exempt
def new_prize(request, item_id=None):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }

    try:
        if not item_id:
            table = Prize()
        else:
            table = Prize.objects.get(pk=item_id)

        table.name = request.POST['name']
        table.image = request.POST['image']
        table.save()
        if table.pk:
            response['status'] = 'ok'
            response['message'] = 'Sus datos han sido guardados exitosamente'

    except IntegrityError:
        print('Duplicate')
        response['message'] = 'Ya existe un registro igual'

    except Exception as e:
        print("Error")
        print(e)

    return JsonResponse(response)


@jwt_check
def delete_prize(request, item_id):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }
    try:
        table = Prize.objects.get(pk=item_id)

        if table:
            table.delete()
            response['status'] = 'ok'
            response['message'] = 'Ha sido borrado exitosamente'

    except Exception as e:
        print e.message

    # Who request are
    # print(request.META.get('HTTP_ORIGIN'))
    return JsonResponse(response)


@jwt_check
def all_teams(request):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }
    try:
        items = Teams.objects.all()
        response['status'] = 'ok'
        response['items'] = []
        del response['message']
        if items:
            for item in items:
                data = {
                    'id': item.id,
                    'name': item.name
                }
                response['items'].append(data)

    except Exception as e:
        print e.message

    return JsonResponse(response)


@jwt_check
@csrf_exempt
def new_team(request, item_id=None):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }

    try:
        if not item_id:
            table = Teams()
        else:
            table = Teams.objects.get(pk=item_id)
            firebase().change_team_name(table.name, request.POST['name'])

        table.name = request.POST['name']
        table.save()
        if table.pk:
            response['status'] = 'ok'
            response['message'] = 'Sus datos han sido guardados exitosamente'

    except IntegrityError as e:
        print('Duplicate')
        print(e)
        response['message'] = 'Ya existe un registro con un nombre similar'

    except Exception as e:
        print("Error")
        print(e)

    return JsonResponse(response)


@jwt_check
def delete_team(request, item_id):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }
    try:
        table = Teams.objects.get(pk=item_id)

        if table:
            table.delete()
            response['status'] = 'ok'
            response['message'] = 'Ha sido borrado exitosamente'

    except Exception as e:
        print e.message

    # Who request are
    # print(request.META.get('HTTP_ORIGIN'))
    return JsonResponse(response)


@jwt_check
def all_users(request):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }
    try:
        items = Users.objects.filter(user__is_active=True)
        response['status'] = 'ok'
        response['items'] = []
        del response['message']
        if items:
            for item in items:
                data = treatment_user(item)
                response['items'].append(data)

    except Exception as e:
        print e.message

    print(response)
    return JsonResponse(response)


@jwt_check
@csrf_exempt
@transaction.atomic
def new_user(request, item_id=None):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }

    try:
        if not item_id:
            print('Creando User')
            created = True
            user = User()
            user.username = request.POST['username']
        else:
            print('Editar User')
            created = False
            user = User.objects.get(pk=item_id)

        password = request.POST['password']
        repassword = request.POST['repassword']

        if created:
            if password != '':
                if password == repassword:
                    user.set_password(password)
                else:
                    response['message'] = 'Contraseñas no coinciden'
                    raise Exception(response['message'])
            else:
                response['message'] = 'La contraseña es requerida'
                raise Exception(response['message'])
        else:
            if password != '':
                if password == repassword:
                    user.set_password(password)
                else:
                    response['message'] = 'Contraseñas no coinciden'
                    raise Exception(response['message'])

        user.first_name = request.POST['first_name']
        user.last_name = request.POST['last_name']
        user.email = request.POST['email']


        user.save()
        if user.id:
            if created:
                print('Creando Users')
                data = Users()
                data.user_id = user.id
            else:
                print('Editando Users ' + str(user.id))
                data = Users.objects.get(user_id=user.id)

            data.position = request.POST['position']
            data.company = request.POST['company']
            data.team_id = request.POST['team[id]']

            data.save()
            if data.pk:
                firebase().create_user(data)

                response['status'] = 'ok'
                response['message'] = 'Sus datos han sido guardados exitosamente'

            raise Exception('Explosito!!!')

    except IntegrityError as e:
        print('Duplicate')
        print(e)
        response['message'] = 'Ya existe un registro con la misma cédula'

    except Exception as e:
        print("Error")
        print(e)

    return JsonResponse(response)


@jwt_check
def delete_user(request, item_id):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }
    try:
        item = User.objects.get(pk=item_id)

        if item:
            item.is_active = False
            item.save()
            firebase().delete_user(item)
            response['status'] = 'ok'
            response['message'] = 'Ha sido borrado exitosamente'

    except Exception as e:
        print e.message

    # Who request are
    # print(request.META.get('HTTP_ORIGIN'))
    return JsonResponse(response)


@jwt_check
def all_cycles(request, for_prize=None):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }
    try:
        if not for_prize:
            cycles = Cycles.objects.all()
        else:
            cycles = Cycles.objects.filter(status=False)

        response['status'] = 'ok'
        response['items'] = []
        del response['message']
        if len(cycles):
            for item in cycles:
                cycle = treatment_cycle(item)
                response['items'].append(cycle)

    except Exception as e:
        print e.message

    return JsonResponse(response)


@csrf_exempt
@transaction.atomic
def new_cycle(request, item_id=None):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }

    post = parser.parse(request.POST.urlencode())

    try:

        check = Cycles.objects.filter(status=True).first()

        if check:
            if check.pk:
                response['message'] = u'Solo se puede tener un ciclo de evaluación activo'
                raise Exception('No se puede crear ciclo')

        from datetime import datetime as date
        if item_id:
            created = False
            cycle = Cycles.objects.get(pk=item_id)
        else:
            created = True
            cycle = Cycles()

        cycle.name = post['name']
        cycle.expire = date.fromtimestamp(int(post['expire'])/1000)

        cycle.save()

        if cycle.pk:
            if not created:
                Goals.objects.filter(cycles=cycle.pk).delete()

            counter = 0
            goals = post['goals']

            for item in goals:
                goal = Goals()
                goal.name = goals[item]['name']
                goal.type = goals[item]['type']
                goal.score = goals[item]['score']
                goal.cycles_id = cycle.pk

                goal.save()
                counter += 1

            if counter == len(goals):
                response['status'] = 'ok'
                response['message'] = 'Sus datos han sido guardados exitosamente'
            else:
                print 'ss'
                raise Exception('Ocurrió algún incoveniente, por favor intente más tarde')
        else:
            print 'sss'
            raise Exception('Hemos tenido incovenientes, por favor intente más tarde')

    except IntegrityError as e:
        print('Duplicate')
        print(e)
        response['message'] = 'Ya existe un registro con la misma cédula'

    except Exception as e:
        print("Error")
        print(e)

    return JsonResponse(response)


@jwt_check
def delete_cycle(request, item_id):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }
    try:
        item = User.objects.get(pk=item_id)

        if item:
            item.is_active = False
            item.save()
            response['status'] = 'ok'
            response['message'] = 'Ha sido borrado exitosamente'

    except Exception as e:
        print e.message

    # Who request are
    # print(request.META.get('HTTP_ORIGIN'))
    return JsonResponse(response)


def expire_cycle(request, item_id=None):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }
    from datetime import date

    try:
        hoy = date.today()

        if not item_id:
            cycle = Cycles.objects.filter(status=1).first()
        else:
            cycle = Cycles.objects.get(pk=item_id)

        if not cycle:
            response['active'] = None
            response['message'] = u'No hay ciclo de evaluación'
            response['days'] = 0
            raise Exception('Ciclos vacios')

        response['status'] = 'ok'
        response['active'] = cycle.name
        diff = cycle.expire.date() - hoy
        if diff.days <= 0:
            cycle.status = False
            cycle.save()

            response['active'] = None
            response['message'] = u'Ciclo de evaluación ha cerrado'
            response['days'] = 0
            response['data'] = treatment_points(cycle.id)

            users = Users.objects.filter(user__is_active=True)
            for user in users:
                print('%s -> %s' % (user, cycle.id))
                points = calc_point_cycle(cycle.id, user)
                firebase().add_points(user.user.email, points)
                print('%s -> %s -> %s' % (user, cycle.id, points))
        else:
            response['message'] = u'Ciclo de evaluación cierra en %s día(s)' % str(diff.days)
            response['days'] = diff.days

    except Exception as e:
        print(e)

    return JsonResponse(response)


def close_cycle(request, cycle_id):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }

    try:
        cycle = Cycles.objects.get(pk=cycle_id)
        cycle.status = 0
        cycle.save()

        response['status'] = 'ok'
        response['message'] = u'Ciclo cerrado con éxito'
        response['data'] = treatment_points(cycle_id)

        users = Users.objects.filter(user__is_active=True)
        print(len(users))
        for user in users:
            print('%s -> %s' % (user, cycle_id))
            points = calc_point_cycle(cycle_id, user)
            firebase().add_points(user.user.email, points)
            print('%s -> %s -> %s' % (user, cycle_id, points))
    except Exception as e:
        print(e)
        print(e.message)

    return JsonResponse(response)


# @jwt_check
def get_cycle(request):
    response = {
        'status': 'error',
        'message': u'No se pudo obtener el ciclo actual'
    }

    try:
        cycle = Cycles.objects.filter(status=True).last()

        if cycle:
            response['status'] = 'ok'
            response['cycle'] = treatment_cycle(cycle)
            del response['message']
        else:
            response['status'] = 'error'
            response['message'] = u'No hay ningún ciclo de evaluación activo'

    except Exception as e:
        print e.message

    return JsonResponse(response)


@jwt_check
def all_rewards(request):
    response = {
        'status': 'error',
        'message': u'Error al consultar datos'
    }

    try:
        rewards = Rewards.objects.all() \
            .values('prize_id', 'prize__name', 'created_at', 'cycle__name', 'cycle_id') \
            .annotate(total=Count('user')).order_by('prize', 'cycle')

        data = []
        if len(rewards):
            for reward in rewards:
                item = {
                    'prize_id': reward['prize_id'],
                    'name': reward['prize__name'],
                    'cycle': reward['cycle__name'],
                    'cycle_id': reward['cycle_id'],
                    'created_at': reward['created_at'],
                    'total': reward['total'],
                }
                data.append(item)

        response['status'] = 'ok'
        del response['message']
        response['items'] = data

    except Exception as e:
        print(e)
        print(e.message)

    return JsonResponse(response)


@jwt_check
def get_target(request):
    response = {
        'status': 'error',
        'message': u'Error al consultar datos'
    }

    try:
        # FIXME: Hacer una mejor solución para esto, me disculpo por ello
        targets = []
        teams = Teams.objects.all()
        for t in teams:
            item = {
                'id': t.id,
                'name': t.name,
                'enabled': False,
                'users': []
            }
            users = Users.objects.filter(team_id=t.id)
            for u in users:
                subitem = {
                    'id': u.user_id,
                    'name': u.user.first_name + ' ' + u.user.last_name,
                    'enabled': False,
                }
                item['users'].append(subitem)

            targets.append(item)

        response['status'] = 'ok'
        response['items'] = targets
        del response['message']

    except Exception as e:
        print(e)

    return JsonResponse(response)


@csrf_exempt
@jwt_check
@transaction.atomic
def new_awards(request):
    response = {
        'status': 'error',
        'message': u'Error al guardar los datos'
    }

    try:
        user = request.META['JWT_AUTH']

        creator = user['id']
        post = parser.parse(request.POST.urlencode())
        winners = post['users']['']
        cycles = post['cycle']
        prizes = post['prize']
        text = post['text']

        if not len(winners):
            raise Exception('No se recibio ninguna persona ganadora')

        for user_id in winners:
            award = Rewards()
            award.user_id = user_id
            award.prize_id = prizes
            award.text = text
            award.creator_id = creator
            award.cycle_id = cycles
            award.save()

        devices = []
        lista = Devices.objects.all()
        if len(lista) == 1:
            if unicode(lista[0].user_id) in winners:
                devices.append(lista[0].token)
        else:
            for device in lista:
                if device.user_id in winners:
                    devices.append(device.token)

        if len(devices) != 0:
            t = Prize.objects.get(pk=prizes)
            send(devices, text, title=t.name, action='prize')

        response['status'] = 'ok'
        response['message'] = u'Datos del premio guardado con éxito'

    except Exception as e:
        print(e)
        print(e.message)

    return JsonResponse(response)


@jwt_check
def reward_detail(request, prizes_id, cycles_id):
    response = {
        'status': 'error',
        'message': u'Error al consultar datos'
    }

    rewards = Rewards.objects.filter(prize_id=prizes_id, cycle_id=cycles_id)

    if len(rewards):
        data = {
            'prize': rewards[0].prize.name,
            'cycle': rewards[0].cycle.name,
            'created_at': rewards[0].created_at,
            'creator': rewards[0].creator.get_full_name(),
            'users': []
        }
        for reward in rewards:
            person = {
                'name': reward.user.user.get_full_name(),
                'company': reward.user.get_company_display(),
                'team': reward.user.team.name
            }
            data['users'].append(person)

        del response['message']
        response['status'] = 'ok'
        response['data'] = data

    return JsonResponse(response)


@jwt_check
@csrf_exempt
@transaction.atomic
def save_answer(request):
    response = {
        'status': 'error',
        'message': u'Error al guardar los datos'
    }

    try:
        user = request.META['JWT_AUTH']
        goal_id = request.POST['goal']
        answer = request.POST['answer']

        person = Answers()
        person.user_id = user['id']
        person.answer = answer
        person.goal_id = goal_id
        person.save()

        if user['position'] == 2:
            group = GroupAnswers()
            group.answer = answer
            group.team_id = user['team']
            group.goal_id = goal_id
            group.save()

            if not group.pk:
                response['message'] = 'Problemas al almacenar respuesta del grupo'
                raise Exception(response['message'])

        if person.pk:
            response = {
                'status': 'ok',
                'message': u'Su respuesta ha sido almacenada correctamente'
            }
        else:
            response['message'] = 'Problemas al almacenar respuesta'
            raise Exception(response['message'])

    except Exception as e:
        print(e)
        print(e.message)

    return JsonResponse(response)


@jwt_check
@csrf_exempt
def upload_image(request):
    response = {
        'status': 'error',
        'message': u'Error al consultar datos'
    }
    try:
        if request.FILES:
            content_type = ['image/jpeg', 'image/jpg']
            model = Images()
            data = request.FILES.get('image')
            if data.content_type in content_type:
                model.image = data
                model.save()

                response = {
                    'status': 'ok',
                    'message': u'Imagen subida con éxito',
                    'name': '%s' % model.image.url,
                    'url': '%s://%s/%s' % (request._get_scheme(), request.get_host(), model.image.url)
                }
            else:
                response['message'] = 'Formato no aceptado, solo puede ser un .jpg'
        else:
            response['message'] = 'No hay post'
    except Exception as e:
        print(e)
        print(e.message)

    return JsonResponse(response)


# Stats
@jwt_check
def stats_person(request, cycles_id):
    complete = 0
    incomplete = 0
    total = len(Users.objects.exclude(position=3))
    goal_user = Answers.objects.filter(goal__cycles_id=cycles_id).exclude(user__users__position=3).values('user_id')\
        .annotate(total=Count('user_id'))\
        .order_by('total')

    if goal_user == 1:
        if goal_user[0]['total'] == 7:
            complete += 1
        else:
            incomplete += 1
    elif goal_user > 1:
        for user in goal_user:
            if user['total'] == 7:
                complete += 1
            else:
                incomplete += 1

    none = total - (complete + incomplete)

    items = {
        'Completos': complete,
        'Parcial': incomplete,
        'Sin Evaluación': none
    }

    return JsonResponse(items)


@jwt_check
def stats_goals(request, cycles_id):
    count = len(Users.objects.exclude(position=3))

    goal_user = Goals.objects\
        .annotate(total=Count('answers'))\
        .filter(cycles_id=cycles_id)\
        .order_by('id')

    items = {
        'labels': [],
        'nums': [],
        'max': count
    }

    text = 'Meta %d'
    number = 1
    for goal in goal_user:
        items['labels'].append(text % number)
        items['nums'].append(goal.total)
        number += 1

    return JsonResponse(items)


# @jwt_check
def stats_first(request, cycles_id, team_id=None):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }

    try:
        cycle = Cycles.objects.filter(status=True).first()

        if not cycle:
            response['message'] = u'Este ciclo no existe'
            raise(response['message'])

        answer = Answers.objects.filter(goal__cycles_id=cycles_id).order_by('id').first()
        # if not team_id:
        #     answer = Answers.objects.filter(goal__cycles_id=cycles_id).order_by('id').first()
        # else:
        #     answer = Answers.objects.filter(goal__cycles_id=cycles_id, user__users__team_id=team_id)\
        #         .order_by('id')\
        #         .first()

        if not answer:
            response['message'] = u'Nadie aún se ha evaluado para este ciclo'
        else:
            response['status'] = 'ok'
            del response['message']
            response['person'] = {
                'id': answer.user.id,
                'name': answer.user.users.full_name,
                'date': answer.created_at
            }
    except Exception as e:
        print(e)

    return JsonResponse(response)


@jwt_check
def last_answer(request, cycles_id, team_id=None):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }
    count = 10

    cycle = Cycles.objects.get(id=cycles_id)
    if not team_id:
        answer = Answers.objects.filter(goal__cycles_id=cycles_id).order_by('-created_at')[:count]
    else:
        answer = Answers.objects.filter(goal__cycles_id=cycles_id, user__users__team_id=team_id)\
                    .order_by('-created_at')[:count]

    response['status'] = 'ok'
    if not cycle:
        response['message'] = u'No hay un ciclo activo'
    else:
        if not answer and cycle.status:
            response['message'] = u'Nadie aún ha comenzado con las evaluaciones'
        else:
            del response['message']
            response['answers'] = []
            for row in reversed(answer):
                person = {
                    'id': row.user.id,
                    'name': row.user.users.full_name,
                    'goal': row.goal.name,
                    'date': row.created_at
                }
                response['answers'].append(person)

    return JsonResponse(response)


@jwt_check
@csrf_exempt
def update_avatar(request):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }

    try:
        auth = request.META['JWT_AUTH']
        user = Users.objects.get(user__id=auth['id'])
        user.avatar = request.POST['url']
        user.save()

        firebase().update_avatar(auth['email'], user.avatar)

        response['status'] = 'ok'
        response['message'] = 'Avatar actualizado'

    except Exception as e:
        print(e)
        print(e.message)

    return JsonResponse(response)


@jwt_check
@csrf_exempt
def update_password(request):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }
    try:
        user_id = request.META['JWT_AUTH']['id']
        user = User.objects.get(id=user_id)

        password = request.POST['password']
        repassword = request.POST['repassword']

        if password != '':
            if password == repassword:
                user.set_password(password)
            else:
                response['message'] = 'Contraseñas no coinciden'
                raise Exception(response['message'])
        else:
            response['message'] = 'La contraseña es requerida'
            raise Exception(response['message'])

        user.save()
        response['status'] = 'ok'
        response['message'] = 'Contraseña actualizada'
    except Exception as e:
        print(e)
        print(e.message)

    return JsonResponse(response)


def user_answer(request, cycles_id=None):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }

    try:
        if not cycles_id:
            c = Cycles.objects.filter(status=True).order_by('-created_at').first()
            if not cycles_id:
                cycles_id = Cycles.objects.order_by('-created_at').first().id
            else:
                cycles_id = c.id

        del response['message']
        response['status'] = 'ok'
        response['active'] = cycles_id
        response['data'] = treatment_points(cycles_id)

    except Exception as e:
        print(e)
        print(e.message)

    return JsonResponse(response)


def answer_indicted(request, user_id, cycles_id):
    response = {
        'status': 'error',
        'message': u'No se pudo acceder a los datos'
    }

    user = User.objects.get(pk=user_id)
    answers = Answers.objects.filter(user__id=user_id).filter(goal__cycles_id=cycles_id)

    if answers:
        answers.update(indicted=True)
        calc_point_cycle(cycles_id, user)
        response['status'] = 'ok'
        response['message'] = u'Evaluación procesada exitosamente'

    return JsonResponse(response)


def innovation(request, cycle_id=None):
    response = {
        'status': 'error',
        'message': u'Innovación aún no ha sido evaluada'
    }

    try:
        if not cycle_id:
            cycle_id = Cycles.objects.last().id

        item = Innovation.objects.get(user_id=uid, cycle_id=cycle_id)
        response['status'] = 'ok'
        response['process'] = item.process
        response['supervisor'] = item.supervisor
        response['company'] = item.company
        del response['message']

    except Exception as e:
        print e.message

    return JsonResponse(response)


@jwt_check
@csrf_exempt
def new_innovation(request, cycle_id=None):
    response = {
        'status': 'error',
        'message': u'No se pudo almacenar datos intente más tarde'
    }

    user_id = request.META['JWT_AUTH']['id']

    try:
        if request.POST:
            process = request.POST['process']
            supervisor = request.POST['supervisor']
            company = request.POST['company']

            if not cycle_id:
                cycle_id = Cycles.objects.last().id

            item = Innovation.objects.get(user_id=user_id, cycle_id=cycle_id)
            item.process = process
            item.supervisor = supervisor
            item.company = company

            item.save()
            del response['message']

            response['status'] = 'ok'
            response['innovation'] = {
                'process': process,
                'supervisor': supervisor,
                'company': company
            }

    except ObjectDoesNotExist:
        item = Innovation(user_id=user_id, cycle_id=cycle_id)
        item.process = process
        item.supervisor = supervisor
        item.company = company

        item.save()
        del response['message']
        response['status'] = 'ok'
        response['innovation'] = {
            'process': process,
            'supervisor': supervisor,
            'company': company
        }

    except Exception as e:
        print e.message

    return JsonResponse(response)


@jwt_check
def improvement(request, cycle_id=None):
    response = {
        'status': 'error',
        'message': u'La iniciativa de crecimiento aún no ha sido evaluada'
    }

    user_id = request.META['JWT_AUTH']['id']

    try:
        if not cycle_id:
            cycle_id = Cycles.objects.last().id

        item = Improvements.objects.get(user_id=user_id, cycle_id=cycle_id)
        grow = {
            'job': item.job,
            'justify': item.justify,
            'course': item.courses
        }
        response['status'] = 'ok'
        response['grow'] = grow
        del response['message']
    except Exception as e:
        print e.message

    return JsonResponse(response)


@jwt_check
@csrf_exempt
def new_improvement(request, cycle_id=None):
    response = {
        'status': 'error',
        'message': u'No se pudo almacenar datos intente más tarde'
    }

    user_id = request.META['JWT_AUTH']['id']

    try:
        if request.POST:
            if not cycle_id:
                cycle_id = Cycles.objects.last().id

            item = Improvements.objects.get(user_id=user_id, cycle_id=cycle_id)
            item.job = request.POST['job']
            item.justify = request.POST['justify']
            item.courses = request.POST['course']
            item.save()
            del response['message']

            response['status'] = 'ok'
            response['grow'] = {
                'job': request.POST['job'],
                'justify': request.POST['justify'],
                'course': request.POST['course']
            }

    except ObjectDoesNotExist:
        item = Improvements(user_id=user_id, cycle_id=cycle_id)
        item.job = request.POST['job']
        item.justify = request.POST['justify']
        item.courses = request.POST['course']
        item.save()
        del response['message']
        grow = {
            'job': request.POST['job'],
            'justify': request.POST['justify'],
            'course': request.POST['course']
        }
        response['status'] = 'ok'
        response['grow'] = grow


    except Exception as e:
        print e.message

    return JsonResponse(response)
