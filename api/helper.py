# -*- coding: utf-8 -*-
# from backend.firebase import get, put

from firebase import firebase as db
from backend.settings import FCM as config
from motivate.models import Users

RANKING = 'ranking'


class firebase:
    _cursor = None
    def __init__(self):
        self._cursor = db.FirebaseApplication(config['databaseURL'], None)

    def _key(self, email):
        return email.replace('.', '_')

    def create_user(self, user):
        url = '%s/%s/' % (RANKING, self._key(user.user.email))
        data = self._cursor.get(url, None)

        if not data:
            data = {
                'image' : '',
                'likes_count' : 0,
                'name' : user.user.get_full_name(),
                'points' : 0,
                'team' : user.team.name
            }
        else:
            data['name'] = user.user.get_full_name()
            data['team'] = user.team.name

        self._cursor.put(RANKING, self._key(user.user.email), data)

    def delete_user(self, user):
        self._cursor.delete(RANKING, self._key(user.user.email))

    def update_avatar(self, email, avatar):
        url = '%s/%s/' % (RANKING, self._key(email))
        self._cursor.put(url, 'image', avatar)

    def change_team_name(self, old, new):
        data = self._cursor.get(RANKING, None)
        for key in data:
            if data[key]['team'] == old:
                data[key]['team'] = new
                self._cursor.put(RANKING, key, data[key])


    def add_points(self, email, points):
        url_base = '/%s/' % RANKING
        url = '/%s/%s/' % (RANKING, self._key(email))

        data = self._cursor.get(url, None)
        if data:
            data['points'] += points
            self._cursor.put(url_base, self._key(email), data)