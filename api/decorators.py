# -*-coding: utf-8 -*-
from functools import wraps
from django.utils.decorators import available_attrs
from django.http import JsonResponse
from backend.settings import SECRET_KEY
from jose import jwt


def jwt_check(view_func):
    """
    Decorator for check JWT exist
    :param view_func:
    :return:
    """
    def check(*args, **kwargs):
        request = args[0]
        if 'HTTP_AUTHORIZATION' in request.META:
            try:
                token = str(request.META['HTTP_AUTHORIZATION']).replace('Bearer ', '')
                token_decode = jwt.decode(token, SECRET_KEY)

                if token_decode:

                    # TODO: Agregar argumento para validar el nivel usuario y poner más seguridad
                    """
                    if token_decode['position'] <= level:
                        request.META['JWT_AUTH'] = token_decode
                    else:
                            return  jwt_fail()
                    """

                    request.META['JWT_AUTH'] = token_decode

            except Exception as e:
                print(e.message)
                return jwt_fail()

        else:
            return jwt_fail()

        return view_func(*args, **kwargs)

    return wraps(view_func, assigned=available_attrs(view_func))(check)

def jwt_fail():
    response = {
        'status': 'error',
        'message': u'Debe estar correctamente autenticado'
    }

    return JsonResponse(response, status=401)