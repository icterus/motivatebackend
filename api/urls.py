# -*- coding: utf-8 -*-

from django.conf.urls import url
urlpatterns = [
    url(r'^legal/$', 'api.views.legal', name='legal'),
    url(r'^account/(\d+)/$', 'api.views.account', name='account'),
    url(r'^auth/$', 'api.views.auth', name='auth'),
    url(r'^forget/(\d+)/$', 'api.views.forget', name='forget'),
    url(r'^update_data/(\d+)/$', 'api.views.update_data', name='update_data'),
    url(r'^register_devices/(\S+)/$', 'api.views.register_devices', name='register_devices'),


    url(r'^legal/edit/$', 'api.views.edit_legal'),
    url(r'^prize/$', 'api.views.prize', name='prize'),

    url(r'^get_cycles', 'api.views.get_cycle', name='get_cycle'),

    url(r'^goals/$', 'api.views.get_goals', name='get_goals'),

    # Parte nueva del API para la versión 2.0
    url(r'^prizes/$', 'api.views.all_prizes'),
    url(r'^prize/create/$', 'api.views.new_prize'),
    url(r'^prize/(\d+)/edit/$', 'api.views.new_prize'),
    url(r'^prize/(\d+)/delete/$', 'api.views.delete_prize'),

    url(r'^teams/$', 'api.views.all_teams'),
    url(r'^team/create/$', 'api.views.new_team'),
    url(r'^team/(\d+)/edit/$', 'api.views.new_team'),
    url(r'^team/(\d+)/delete/$', 'api.views.delete_team'),

    url(r'^users/$', 'api.views.all_users'),
    url(r'^user/create/$', 'api.views.new_user'),
    url(r'^user/edit/avatar/$', 'api.views.update_avatar'),
    url(r'^user/edit/password/$', 'api.views.update_password'),
    url(r'^user/(\d+)/edit/$', 'api.views.new_user'),
    url(r'^user/(\d+)/delete/$', 'api.views.delete_user'),

    url(r'^cycles/$', 'api.views.all_cycles'),
    url(r'^cycles/(\w+)/$', 'api.views.all_cycles'),
    url(r'^cycle/create/$', 'api.views.new_cycle'),
    url(r'^cycle/(\d+)/edit/$', 'api.views.new_cycle'),
    url(r'^cycle/(\d+)/expire/$', 'api.views.expire_cycle'),
    url(r'^cycle/(\d+)/close/$', 'api.views.close_cycle'),
    url(r'^cycle/expire/$', 'api.views.expire_cycle'),

    url(r'^rewards/$', 'api.views.all_rewards'),
    url(r'^reward/(\d+)/(\d+)/detail/$', 'api.views.reward_detail'),
    url(r'^reward/create/$', 'api.views.new_awards'),
    url(r'^targets/$', 'api.views.get_target'),

    url(r'^answer/create/$', 'api.views.save_answer'),

    url(r'^stats/(\d+)/$', 'api.views.stats_person'),
    url(r'^stats/(\d+)/goals/$', 'api.views.stats_goals'),
    url(r'^stats/(\d+)/first/$', 'api.views.stats_first'),
    url(r'^stats/(\d+)/(\d+)/first/$', 'api.views.stats_first'),
    url(r'^stats/(\d+)/last/$', 'api.views.last_answer'),
    url(r'^stats/(\d+)/(\d+)/last/$', 'api.views.last_answer'),

    url(r'goals/answer/$', 'api.views.user_answer'),
    url(r'goals/answer/(\d+)/$', 'api.views.user_answer'),
    url(r'goals/indicted/(\d+)/(\d+)/$', 'api.views.answer_indicted'),

    url(r'^upload_image/$', 'api.views.upload_image'),


    url(r'innovation/$', 'api.views.innovation'),
    url(r'innovation/(\d+)/$', 'api.views.innovation'),
    url(r'innovation/new/$', 'api.views.new_innovation'),
    url(r'innovation/(\d+)/new/$', 'api.views.new_innovation'),

    url(r'improvement/$', 'api.views.improvement'),
    url(r'improvement/(\d+)/$', 'api.views.improvementn'),
    url(r'improvement/new/$', 'api.views.new_improvement'),
    url(r'improvement/(\d+)/new/$', 'api.views.new_improvement'),
]
